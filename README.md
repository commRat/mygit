# mygit

## Description

Mygit is a tool that simplifies working with the git.
The target group are beginner developers or experienced developers who want to make their work easier (faster).

For example instead of this:

    git add .
    git commit -m "initial commit"
    git push origin master

You are able to write just one line, with the same result:

    mygit push "initial commit"

## Don't you want to install straight away? Try it with docker

Clone this repository, build docker image. Run container & after that You will find yourself in home/mygit directory.
Goal is to create testing repository, where You can test mygit:

    cd ..
    mkdir test
    cd test
    touch new_file

If You executed lines above, You are at home/test, where is empty new_file which you created. Now You should init repository (if you pass link to remote, It will automaticaly set this link as remote: origin):

    mygit init https://gitlab.com/commRat/mygit

Be aware, that You are in docker container, where is not git setting, so You have to add your email & name for git:

    mygit user <email> <name>

Example:

    mygit user john@doe.com John Doe

You have set up git global user successfully. Now You can continue with commit.

    mygit commit "initial commit"

Push it:

    mygit push origin master

You successfully set up Your repo, add file, commit It and push It to remote repository. Now You can try what else can You do with mygit. This command will show some options:

    mygit --help

Have fun :)
