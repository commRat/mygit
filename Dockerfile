FROM fedora
RUN dnf install -y python3-pip python3-setuptools python3-docopt git rpm-build
RUN pip install --upgrade setuptools
RUN mkdir home/mygit
WORKDIR home/mygit
COPY . .
RUN sudo python3 setup.py bdist_rpm
RUN rpm -i dist/mygit-1.1-1.noarch.rpm
