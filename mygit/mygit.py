#!/usr/bin/python

'''
mygit.

Usage:
  mygit [options]
  mygit init [<url>]
  mygit init [<remote_name>] [<url>]
  mygit clone <url>
  mygit status
  mygit log
  mygit lg
  mygit remote
  mygit branch
  mygit diff
  mygit add [<files>]...
  mygit commit <message>
  mygit push <remote> <branch>
  mygit push <message>
  mygit user <email> <name>...

Options:
  -h --help  Show this screen.
  --version  Show version.

Examples:
  mygit init gitlab_remote https://gitlab.com/commRat/mygit
  mygit clone https://gitlab.com/spack/spack
  mygit add setup.py README.md project/script.py
  mygit commit "initial commit"
  mygit push origin master
  mygit push "initial commit"
  mygit status
  myigt log
'''


from docopt import docopt
import subprocess
import sys
import pkg_resources


def execute_command(command):
    subprocess.run(command, shell=True)
    return 0


def init_url_with_name(remote_name, url):
    init_without_url()
    return execute_command(f"git remote add {remote_name} {url}")


def init_url_origin(url):
    init_without_url()
    return execute_command(f"git remote add origin {url}")


def init_without_url():
    return execute_command("git init")


def git_clone(url):
    return execute_command(f"git clone {url}")


def status():
    return execute_command("git status")


def standard_log():
    return execute_command("git log")


def improved_log():
    command = "git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) " \
    "- %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'"
    return execute_command(command)


def show_remotes():
    return execute_command("git remote -v")


def show_branches():
    return execute_command("git branch -a")


def show_diff():
    return execute_command("git diff")


def add_files(files):
    if files:
        for file in files:
            execute_command(f"git add {file}")
        return 0
    return execute_command("git add .")


def commit(message):
    return execute_command(f"git commit -m '{message}'")


def standard_push(remote, branch):
    return execute_command(f"git push {remote} {branch}")


def improved_push(message):
    add_files(0)
    commit(message)
    return standard_push("origin", "master")


def config_user(email, name):
    final_name = ""
    for word in name:
        final_name += f"{word} "
    execute_command(f"git config --global user.name '{final_name}'")
    execute_command(f"git config --global user.email '{email}'")


def main():
    args = docopt(__doc__)
    if args["--version"]:
        version = pkg_resources.resource_string(__name__, "VERSION").decode().strip()
        return print(version)
    if args["init"] and args["<remote_name>"] and args["<url>"]:
        return init_url_with_name(args["<remote_name>"], args["<url>"])
    if args["init"] and args["<url>"]:
        return init_url_origin(args["<url>"])
    if args["init"]:
        return init_without_url()
    if args["clone"]:
        return git_clone(args["<url>"])
    if args["status"]:
        return status()
    if args["lg"]:
        return standard_log()
    if args["log"]:
        return improved_log()
    if args["remote"]:
        return show_remotes()
    if args["branch"]:
        return show_branches()
    if args["diff"]:
        return show_diff()
    if args["add"]:
        return add_files(args["<files>"])
    if args["commit"]:
        return commit(args["<message>"])
    if args["push"] and args["<remote>"] and args["<branch>"]:
        return standard_push(args["<remote>"], args["<branch>"])
    if args["push"] and args["<message>"]:
        return improved_push(args["<message>"])
    if args["user"] and args["<email>"] and args["<name>"]:
        return config_user(args["<email>"], args["<name>"])
    return 0


if __name__ == '__main__':
    sys.exit(main())
